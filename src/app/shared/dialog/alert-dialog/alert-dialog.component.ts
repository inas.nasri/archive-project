import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

export interface AlertDialogData {
  title?: string;
  text?: string;
  cancelButtonText?: string;
  confirmButtonText?: string;
  /* RESULT used with after closed observable  */
  result?: boolean;
}

@Component({
  selector: "app-alert-dialog",
  templateUrl: "./alert-dialog.component.html",
  styleUrls: ["./alert-dialog.component.scss"],
})
export class AlertDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<AlertDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AlertDialogData
  ) {}

  ngOnInit() {
  }

  get title() {
    return this.data && this.data.title ? this.data.title : "هل أنت متأكد؟";
  }

  get text() {
    return this.data && this.data.text ? this.data.text: "انت متأكد انك متأكد؟";
  }

  get cancelButtonText() {
    return this.data && this.data.cancelButtonText ? this.data.cancelButtonText : "إلغاء";
  }

  get confirmButtonText() {
    return this.data && this.data.confirmButtonText ? this.data.confirmButtonText : "تأكيد";
  }

  onConfirm() {

  }

  onCancel() {

  }
}
