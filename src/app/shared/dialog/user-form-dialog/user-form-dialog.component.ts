import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Data } from '@angular/router';

@Component({
  selector: 'app-user-form-dialog',
  templateUrl: './user-form-dialog.component.html',
  styleUrls: ['./user-form-dialog.component.css']
})
export class UserFormDialogComponent implements OnInit {
  
  userForm: FormGroup;
 

  jobs = [
    {value: 'برامج محاسبة', selected: false},
    {value: 'تطبيقات مكتبية', selected: false},
    {value: 'تطبيقات موبايل', selected: false},
    {value: 'مواقع الكترونية', selected: false}
  ];
  selectedColumns = []
  imageFile: File;
  imageSrc: any;
  constructor( public dialogRef: MatDialogRef<UserFormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Data,
    private formBuilder: FormBuilder,
    ) { }

  ngOnInit(): void {
    this.data?this.imageSrc=this.data.image:''
    console.log(this.imageSrc)
    this.userForm =this.buildUserForm();
 
  }

  buildUserForm() {

    const user = this.data ;
    return user
      ? (
        (this.formBuilder.group({
          name: new FormControl(this.data.name, [ Validators.required]),
          place: new FormControl(this.data.place||"", ),
          date:new FormControl(this.data.date, [ Validators.required]),
          email: new FormControl(this.data.email||'', [Validators.required, Validators.email]),
          phone: new FormControl(this.data.phone||'', [ Validators.required,Validators.minLength(9)]),
          summary:new FormControl(this.data.summary||"", [ Validators.required,Validators.minLength(50),Validators.maxLength(300)]),
          jobs:new FormControl(this.data.jobs||"", [ Validators.required]),
          gender:new FormControl(this.data.gender||"", [ Validators.required]),
          image:new FormControl(this.data.image||""),
        })
      )):(
        this.formBuilder.group({
          name: new FormControl("", [ Validators.required]),
          place: new FormControl("", ),
          date:new FormControl("", [ Validators.required]),
          email: new FormControl('', [Validators.required, Validators.email]),
          phone: new FormControl('', [ Validators.required,Validators.minLength(9)]),
          summary:new FormControl("", [ Validators.required,Validators.minLength(50),Validators.maxLength(300)]),
          jobs:new FormControl("", [ Validators.required]),
          gender:new FormControl("", [ Validators.required]),
          image:new FormControl("", ),
        })
        )
  }
  onSave(){
    if(this.userForm.valid){
      this.userForm.get('jobs').setValue(this.selectedColumns)
      console.log(this.userForm.value)
      this.dialogRef.close({data:this.userForm.value});
    }
  }

  changed(){
    this.selectedColumns = this.userForm.get('jobs').value;

  }
  onFileChange(event) {
    const reader = new FileReader();
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
     
      reader.onload = () => {
    
        this.imageSrc = reader.result as string;
      
        this.userForm.get('image').patchValue(
          this.imageSrc
        );
    
      };
    
    }
    }
  
}
  