import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModules } from './material-modules';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
import { UserTableComponent } from './user-table/user-table.component';
import { MatSortModule } from '@angular/material/sort';
import { UserFormDialogComponent } from './dialog/user-form-dialog/user-form-dialog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AlertDialogComponent } from './dialog/alert-dialog/alert-dialog.component';


@NgModule({
  declarations: [
    UserTableComponent,
    UserFormDialogComponent,
    AlertDialogComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatSortModule,
    ...MaterialModules,
  

  ],
  exports:[
    UserTableComponent,
    FormsModule,
    ReactiveFormsModule,
    MatSortModule,
    UserFormDialogComponent,
    AlertDialogComponent,
    ...MaterialModules,
    
  ]
  ,
  providers: [
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 2500 } },
  ],
})
export class SharedModule { }
