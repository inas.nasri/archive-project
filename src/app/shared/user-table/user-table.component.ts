import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { User } from 'src/app/dashboard/users/models/user';
import { UsersService } from 'src/app/dashboard/users/services/users';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})

export class UserTableComponent implements OnInit {
  @Input() users: User[] = []
  @Input() userRole:string=''
  @Output() onEditUser = new EventEmitter()
  @Output() onDeleteUser = new EventEmitter()
  @Input() dataSource
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit(): void {
    
    this.dataSource.filterPredicate = function (record, filter) {
      return record.name.indexOf(filter) != -1;
    }
  }

 
  displayedColumns = ['action', 'phone', 'date', 'email', 'name'];


  /**
   * Set the paginator after the view init since this component will
   * be able to query its view for the initialized paginator.
   */
  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  
}



