import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { AuthService } from 'src/app/auth/components/services/AuthService';
import { User } from '../../users/models/user';
import { UsersService } from '../../users/services/users';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  mode = new FormControl('over');
  users:User[]=[]
  shouldRun = [/(^|\.)plnkr\.co$/, /(^|\.)stackblitz\.io$/].some(h => h.test(window.location.host));
  constructor(private authService:AuthService,private userService:UsersService) {
}
sidebarOpened =true;
  ngOnInit(): void {
    
  
 
    this.users=this.userService.getusers();
    console.log(this.users)
    
  }

  logout(){
    this.authService.logout()
  }

}


/**  Copyright 2020 Google LLC. All Rights Reserved.
    Use of this source code is governed by an MIT-style license that
    can be found in the LICENSE file at http://angular.io/license */
