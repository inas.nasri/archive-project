export interface User {
    name: string;
    email: string;
    date : string;
    phone:string;
    place?:string;
    jobs?:string;
    summary?:string;
    gender?:string;
    image?:string

}