import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserManagerComponent } from './container/user-manager/user-manager.component';

import { SharedModule } from 'src/app/shared/shared.module';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';



@NgModule({
  declarations: [
    UserManagerComponent
  ],
  imports: [
    CommonModule,
    MatSidenavModule,
  	MatToolbarModule,
    SharedModule
  ],
  exports:[UserManagerComponent]
})
export class UserManagerModule { }
