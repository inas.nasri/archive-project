import { Injectable } from "@angular/core";
import { User } from "../models/user";

@Injectable({
    providedIn: 'root'
  })
  export class UsersService {
    private users: User[] = [
        {phone: '1775677', name: 'احمد', date:'12/1/2000', email: 'H@h.com'},
        {phone: '1775677', name: 'محمد', date:'12-1-2000', email: 'p@p.com'},
        {phone: '1775677', name: 'سارة', date:'12-1-2000', email: 'p@p.com'},
        {phone: '1775677', name: 'سلام', date: '12-1-2000', email: 'p@p.com'},
        {phone: '1775677', name: 'محمود', date:'12-1-2000' , email: 'test@test.com'},
        {phone: '6546789', name: 'ايناس', date: '12-1-2000', email: 'test@test.com'},
        {phone: '6546789', name: 'ماسة', date:'12-1-2000' , email: 'test@test.com'},
        {phone: '6546789', name: 'جميل', date: '12-1-2000', email: 'test@test.com'},
        {phone: '6546789', name: 'حامد', date:'12-1-2000' , email: 'test@test.com'},
        {phone: '6546789', name: 'كريم', date:'12-1-2000' , email: 'test@test.com'},
        {phone: '6546789', name: 'طارق', date:'12-1-2000' , email: 'test@test.com'},
        {phone: '6546789', name: 'ماجد', date:'12-1-2000' , email: 'test@test.com'},
        {phone: '6546789', name: 'سليم', date:'12-1-2000' , email: 'test@test.com'},
        {phone: '6546789', name: 'سلمى', date: '12-1-2000', email: 'test@test.com'},
        {phone: '6546789', name: 'مروة', date:'12-1-2000' , email: 'test@test.com'},
        {phone: '6546789', name: 'عمر', date:'12-1-2000', email: 'test@test.com'},
        {phone: '6546789', name: 'ميساء', date:'12-1-2000' , email: 'test@test.com'},
        {phone: '6546789', name: 'روعة', date:'12-1-2000' , email: 'test@test.com'},
        {phone: '6546789', name: 'ليلى', date:'12-1-2000' , email: 'test@test.com'},
        {phone: '6546789', name: 'شادي', date: '12-1-2000', email: 'test@test.com'},
      ];
    constructor() {
      
        }
    
 public getusers(){
   return this.users
 }
}
