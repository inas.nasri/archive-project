import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { element } from 'protractor';
import { AuthService } from 'src/app/auth/components/services/AuthService';
import { LoginCredentials } from 'src/app/auth/models/LoginCredentials';
import { AlertDialogComponent } from 'src/app/shared/dialog/alert-dialog/alert-dialog.component';
import { UserFormDialogComponent } from 'src/app/shared/dialog/user-form-dialog/user-form-dialog.component';
import { User } from '../../models/user';
import { UsersService } from '../../services/users';

@Component({
  selector: 'app-user-manager',
  templateUrl: './user-manager.component.html',
  styleUrls: ['./user-manager.component.css']
})
export class UserManagerComponent implements OnInit {
  users: User[] = []
  dataSource;
  userRole:string
  constructor(private userService: UsersService, private authService: AuthService,
    private dialog: MatDialog, private snackbar: MatSnackBar) { }



  ngOnInit(): void {
    this.users = this.userService.getusers();
    this.dataSource = new MatTableDataSource<User>(this.users);
    this.userRole=JSON.parse(this.authService.getuser()).role
    
        
  }
 /***
  * 
  * create new user on table
  */
  adduser() {

    this.dialog.open(UserFormDialogComponent, {
      height: '55vh',
      width: '50vw'
    }).afterClosed().subscribe(res => {
      if(res){
         this.dataSource.data.push(res.data)
         this.snackbar.open('تمت العملية بنجاح')
      }
     

    });
  }

  /***
  * 
  * Edit user on table
  */
  onEditUser(event) {
    console.log(event)
    this.dialog.open(UserFormDialogComponent, {
      data: event,
      height: '55vh',
      width: '50vw'
    }).afterClosed().subscribe(res => {

      let i = 0
      this.users.forEach(e => {
        e == event ? (this.users.splice(i, 1)) : ''
        i++
      })
      this.users.push(res.data)
      this.dataSource.data = this.users
      this.snackbar.open('تمت العملية بنجاح')
    });
  }

  /***
  * 
  * Delete new user on table
  */
  onDeleteUser(event) {

    this.dialog.open(AlertDialogComponent, {
      data: {
        title: "ستقوم بحذف هذا الزبون",
        text: "هل انت متأكد؟"
      }
    }).afterClosed().subscribe(res => {
      if (res) {
        let i = 0
        this.users.forEach(e => {
          e == event ? this.users.splice(i, 1) : ''
          i++
        })

        this.dataSource.data = this.dataSource.data
        this.snackbar.open('تم حذف المستخدم بنجاح')


      }
    });
  }
}
