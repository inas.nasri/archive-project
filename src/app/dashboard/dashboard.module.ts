import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import {MatSidenavModule} from '@angular/material/sidenav';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { UserManagerComponent } from './users/container/user-manager/user-manager.component';
import { UserManagerModule } from './users/user-manager.module';


@NgModule({
  declarations: [DashboardComponent,
 ],
  imports: [
    CommonModule,
    SharedModule,
    MatSidenavModule,
  	MatToolbarModule,
    DashboardRoutingModule,
    UserManagerModule
       
  ]
})
export class DashboardModule { }
