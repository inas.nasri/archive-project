import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UserManagerComponent } from './users/container/user-manager/user-manager.component';



const routes: Routes = [
  {
    path: "",
    component: DashboardComponent,

    children: [
      { path: '',component: UserManagerComponent},
      // {
      //   path: "user",
      //   // loadChildren: () =>
      //   //   import("./users/user-manager.module").then(
      //   //     (m) => m.UserManagerModule
      //   //   ),
      //   // data: {
      //   //   title: "لوحة تحكم المستخدمين",
      //   // },
      // },
    ]
  }
]
  

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule { }
