export interface LoginCredentials {
    email: string;
    password: string;
    role: string ;
}