import { Injectable } from "@angular/core";
import { LoginCredentials } from "../../models/LoginCredentials";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() {
  }
  public login(credentials: LoginCredentials) {
    this.getusers().map(user =>(user.email==credentials.email&&user.password==credentials.password?
    localStorage.setItem("userdashboard", 
    JSON.stringify({ email: credentials.email, password: credentials.password ,role:user.role })
    ):''
      
    )) 
    
  }
  public logout(){
    localStorage.removeItem("userdashboard")
  }
  getemail() {
    return 'admin@admin.com'
  }
  getpassword() {
    return '123456789a'
  }
  getusers(){
    return [{email:'user1@user1.com',password:'12345a',role:'user'},
            {email:'user2@user2.com',password:'12345a',role:'user'},
            {email:'admin@admin.com',password:'123456789a',role:'admin'}] as LoginCredentials[]
  }
  public getuser() {
    return localStorage.getItem('userdashboard') ;
  }

  
}