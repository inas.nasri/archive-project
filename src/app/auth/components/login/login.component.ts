

import { MatSnackBar } from "@angular/material/snack-bar";
import {
  FormControl,
  Validators,
  FormGroup,
  ValidationErrors,
  EmailValidator,
} from "@angular/forms";
import { Component, OnInit } from "@angular/core";
import { LoginCredentials } from "../../models/LoginCredentials";
import { AuthService } from "../services/AuthService";
import { Router } from "@angular/router";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private _snackbar: MatSnackBar,private AuthService:AuthService,private router:Router) {
  }

  
  ngOnInit() {
    if(this.AuthService.getuser())
    this.router.navigateByUrl('dashboard')
    this.loginForm = new FormGroup({
      email: new FormControl("", [Validators.email, Validators.required]),
      password: new FormControl("", [
        Validators.required,
        Validators.minLength(6),
        Validators.pattern(".*[a-zA-Z].*"),
      ]),
      rememberMe: new FormControl(true),
    });
  }

  onSubmit() {
    const credentials: LoginCredentials = this.loginForm.value;
    console.log(this.AuthService.login(credentials))
    this.AuthService.login(credentials)
    this.AuthService.getuser()?(this.router.navigateByUrl('dashboard'),
    this._snackbar.open("تم تسجيل الدخول بنجاح") ):
    this._snackbar.open('خطأ في تسجيل الدخول ')
      
    
  }
}
